package id.co.ahm.training.gcm.samplegcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private EditText tName;
    private EditText tGcmId;
    private Button btnRegister;
    private Button btnUnRegister;

    private GoogleCloudMessaging gcm;

    // Untuk menyimpan gcmId yg sudah diregister
    private SharedPreferences sharedPreference;

    private static final String GCM_PROJECT_ID = "52919407469";
    private static final String PREFERENCE_NAME = "gcm.training";

    private static final String NAME_PREFERENCE = "NAME";
    private static final String GCM_ID_PREFERENCE = "GCM_ID";

    private static final String BACKEND_URL = "http://128.199.92.99:8080/notif/rest/gcm/";
    private static final String BACKEND_URL_REGISTER = BACKEND_URL + "register";
    private static final String BACKEND_URL_UNREGISTER = BACKEND_URL + "unregister";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreference = this.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        gcm = GoogleCloudMessaging.getInstance(this);

        tName = (EditText) findViewById(R.id.txtName);
        tGcmId = (EditText) findViewById(R.id.txtGcmId);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnUnRegister = (Button) findViewById(R.id.btnUnRegister);

        checkStoredGcm();

        // button action
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               registerGcm();
            }
        });


        // button action
        btnUnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unregisterGcm();
            }
        });
    }

    private void checkStoredGcm(){
        String storedGcmId = getFromPreference(GCM_ID_PREFERENCE);
        if(storedGcmId != null){
            String storedName = getFromPreference(NAME_PREFERENCE);

            tName.setText(storedName);
            tGcmId.setText(storedGcmId);

            btnUnRegister.setEnabled(true);
            btnRegister.setEnabled(false);

        } else {

            tName.setText("");
            tGcmId.setText("");

            btnUnRegister.setEnabled(false);
            btnRegister.setEnabled(true);
        }
    }

    private void registerGcm(){

        if(tName.getText().toString().isEmpty()){
            showAlert("Name harus diisi");
            return;
        }

        AsyncTask<Void, Void, String> registerTask = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                try {
                    String regId = gcm.register(GCM_PROJECT_ID);
                    Log.d(TAG, "GCM Reg ID: " + regId);
                    return regId;
                } catch (IOException e) {
                    Log.e(TAG, "GCM Reg failed : " + e.getMessage());
                    showAlert("Register GCM gagal");
                }
                return null;

            }

            @Override
            protected void onPostExecute(String result) {
                if(result != null && !result.isEmpty()) {

                    tGcmId.setText(result);
                    // save gcmId to preference
                    savePreference(NAME_PREFERENCE, tName.getText().toString());
                    savePreference(GCM_ID_PREFERENCE, result);

                    btnUnRegister.setEnabled(true);
                    btnRegister.setEnabled(false);

                    sendGcmToBackend(result);
                }
            }
        };

        registerTask.execute();
    }

    private void unregisterGcm(){

        AsyncTask<Void, Void, Void> registerTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    gcm.unregister();
                    Log.d(TAG, "GCM unReg success ");
                } catch (IOException e) {
                    Log.e(TAG, "GCM unReg failed : " + e.getMessage());
                    showAlert("UnRegister GCM gagal");
                }
                return null;

            }

            @Override
            protected void onPostExecute(Void result) {
                String storedGcmId = getFromPreference(GCM_ID_PREFERENCE);
                removeGcmFromBackend(storedGcmId);

                sharedPreference.edit().clear().apply();

                tName.setText("");
                tGcmId.setText("");

                btnUnRegister.setEnabled(false);
                btnRegister.setEnabled(true);
            }
        };

        registerTask.execute();
    }

    private void savePreference(String key, String value){
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(key, value);
        editor.apply();
    }

    private String getFromPreference(String key){
        return sharedPreference.getString(key, null);
    }

    private void sendGcmToBackend(final String gcmId){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", tName.getText().toString());
            jsonObject.put("gcmKey", gcmId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        VolleyClient.getInstance(this).post(BACKEND_URL_REGISTER, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "success send gcmId to backend : "+response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "failed send gcmId to backend : "+error.getMessage());
                    }
                });

    }

    private void removeGcmFromBackend(final String gcmId){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", tName.getText().toString());
            jsonObject.put("gcmKey", gcmId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        VolleyClient.getInstance(this).post(BACKEND_URL_UNREGISTER, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "success remove gcmId to backend : "+response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "failed remove gcmId to backend : "+error.getMessage());
                    }
                });
    }

    /**
     * show toast alert
     * @param message
     */
    private void showAlert(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}
