package id.co.ahm.training.gcm.samplegcm;

import android.content.Context;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.lang.reflect.Method;

/**
 * Volley rest client
 * Created by andri.khrisharyadi@gmail
 * on 3/9/15.
 */
public class VolleyClient {

    private static VolleyClient instance;
    private RequestQueue requestQueue;

    private Gson gson = new Gson();

    private VolleyClient(Context context) {

        requestQueue = Volley.newRequestQueue(context);

    }

    public static VolleyClient getInstance(Context context) {
        if (instance == null) {
            instance = new VolleyClient(context);
        }
        return instance;
    }

    public RequestQueue getRequestQueue() {
        return requestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag("VolleyClient");
        getRequestQueue().add(req);
    }



    public void post(String url, JSONObject jsonObject, Response.Listener<JSONObject> reponseListener, Response.ErrorListener errorListener){

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, reponseListener, errorListener);
        getRequestQueue().add(jsonObjectRequest);
    }


}
